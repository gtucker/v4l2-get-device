/*
 * Copyright © 2018, 2019 Collabora Ltd.
 *
 * Based on v4l2-sysfs-path by Mauro Carvalho Chehab:
 *
 * Copyright © 2011 Red Hat, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of Red Hat
 * not be used in advertising or publicity pertaining to distribution
 * of the software without specific, written prior permission.  Red
 * Hat makes no representations about the suitability of this software
 * for any purpose.  It is provided "as is" without express or implied
 * warranty.
 *
 * THE AUTHORS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 * NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <argp.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/unistd.h>

#include <linux/videodev2.h>

#include <libmedia_dev/get_media_devices.h>

const char *argp_program_version = "v4l2-get-device 1.0";
const char *argp_program_bug_address = "Ezequiel Garcia <ezequiel@collabora.com>";

struct args {
	const char *driver;
	unsigned int device_caps;
	const char *cap_format;
	const char *out_format;
};

static const struct argp_option options[] = {
	{"driver", 'd', "DRIVER", 0, "device driver name", 0},
	{"capture-format", 'f', "FORMAT", 0, "capture format name", 0},
	{"output-format", 'F', "FORMAT", 0, "output format name", 0},
	{"v4l2-device-caps", 'c', "CAPS", 0, "v4l2 device capabilities", 0},
	{ 0 }
};

// Adapted from v4l-utils's v4l2-info.cpp
// The char array passed in should be at least 8 characters long to fit the
// string.
static void fcc2s(unsigned int val, char *s)
{
	s[0] = val & 0x7f;
	s[1] = (val >> 8) & 0x7f;
	s[2] = (val >> 16) & 0x7f;
	s[3] = (val >> 24) & 0x7f;
	if (val & (1U << 31)) {
		s[4] = '-';
		s[5] = 'B';
		s[6] = 'E';
		s[7] = 0;
	} else {
		s[4] = 0;
	}
}

static unsigned int parse_capabilities(const char *arg)
{
	char *s, *str = strdup(arg);
	unsigned int caps = 0;

	s = strtok (str,",");
	while (s != NULL) {
		if (!strcmp(s, "V4L2_CAP_VIDEO_CAPTURE"))
			caps |= V4L2_CAP_VIDEO_CAPTURE;
		else if (!strcmp(s, "V4L2_CAP_VIDEO_OUTPUT"))
			caps |= V4L2_CAP_VIDEO_OUTPUT;
		else if (!strcmp(s, "V4L2_CAP_VIDEO_OVERLAY"))
			caps |= V4L2_CAP_VIDEO_OVERLAY;
		else if (!strcmp(s, "V4L2_CAP_VBI_CAPTURE"))
			caps |= V4L2_CAP_VBI_CAPTURE;
		else if (!strcmp(s, "V4L2_CAP_VBI_OUTPUT"))
			caps |= V4L2_CAP_VBI_OUTPUT;
		s = strtok (NULL, ",");
	}
	free(str);
	return caps;
}

static error_t parse_opt(int k, char *arg, struct argp_state *state)
{
	struct args *args = state->input;

	switch (k) {
	case 'd':
		args->driver = arg;
		break;
	case 'c':
		args->device_caps = parse_capabilities(arg);
		break;
	case 'f':
		args->cap_format = arg;
		break;
	case 'F':
		args->out_format = arg;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

enum video_type {
	TYPE_CAPTURE,
	TYPE_OUTPUT,
};

// Adapted from v4l-utils's v4l2-ctl.cpp
static int get_format_fcc_string(int fd, const struct v4l2_capability *cap,
				 enum video_type type, char *fcc_string)
{
	struct v4l2_format vfmt;
	unsigned int is_multiplanar;
	unsigned int vidcap_buftype;
	unsigned int vidout_buftype;;
	int priv_magic;
	int ret;

	priv_magic = (cap->device_caps & V4L2_CAP_EXT_PIX_FORMAT) ?
		     V4L2_PIX_FMT_PRIV_MAGIC : 0;
	is_multiplanar = cap->device_caps & (V4L2_CAP_VIDEO_CAPTURE_MPLANE
					    | V4L2_CAP_VIDEO_M2M_MPLANE
					    | V4L2_CAP_VIDEO_OUTPUT_MPLANE);
	vidcap_buftype = is_multiplanar ? V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE
					: V4L2_BUF_TYPE_VIDEO_CAPTURE;
	vidout_buftype = is_multiplanar ? V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE :
					  V4L2_BUF_TYPE_VIDEO_OUTPUT;
	memset(&vfmt, 0, sizeof(vfmt));
	vfmt.fmt.pix.priv = priv_magic;
	vfmt.type = (type == TYPE_CAPTURE) ? vidcap_buftype : vidout_buftype;
	ret = ioctl(fd, VIDIOC_G_FMT, &vfmt);
	if (ret)
		return ret;

	fcc2s(vfmt.fmt.pix.pixelformat, fcc_string);

	return 0;
}

static struct argp argp = {
	.options = options,
	.parser = parse_opt,
};

int main(int argc, char *argv[])
{
	const char *vid;
	struct args args;
	void *md;

	args.driver = NULL;
	args.device_caps = 0;
	args.cap_format = NULL;
	args.out_format = NULL;
	argp_parse(&argp, argc, argv, 0, 0, &args);

	if ((args.driver == NULL) && !args.device_caps &&
	    (args.cap_format == NULL) && (args.out_format == NULL)) {
		printf("Invalid arguments.  Please run:\n");
		printf("    v4l2-get-device --help\n");
		return -1;
	}

	md = discover_media_devices();

	vid = NULL;
	do {
		struct v4l2_capability cap;
		char devnode[64];
		// We need a char array of size >= 8 to pass to fcc2s()
		char fcc_string[8];
		int ret;
		int fd;

		vid = get_associated_device(md, vid, MEDIA_V4L_VIDEO,
					    NULL, NONE);
		if (!vid)
			break;
		snprintf(devnode, 64, "/dev/%s", vid);
		fd = open(devnode, O_RDWR);
		if (fd < 0)
			continue;

		memset(&cap, 0, sizeof cap);
		ret = ioctl(fd, VIDIOC_QUERYCAP, &cap);
		if (ret) {
			close(fd);
			continue;
		}

		if (args.cap_format) {
			ret = get_format_fcc_string(fd, &cap, TYPE_CAPTURE, fcc_string);
			if (ret) {
				close(fd);
				continue;
			}
			if (strncmp(args.cap_format, fcc_string, sizeof(fcc_string))) {
				close(fd);
				continue;
			}
		}

		if (args.out_format) {
			ret = get_format_fcc_string(fd, &cap, TYPE_OUTPUT, fcc_string);
			if (ret) {
				close(fd);
				continue;
			}
			if (strncmp(args.out_format, fcc_string, sizeof(fcc_string))) {
				close(fd);
				continue;
			}
		}

		close(fd);

		if (args.driver &&
		    strncmp(args.driver, (char *)cap.driver,
			    sizeof(cap.driver)))
			continue;

		if (args.device_caps &&
		    (args.device_caps & cap.device_caps) != args.device_caps)
			continue;

		fprintf(stdout, "%s\n", devnode);
	} while (vid);
	free_media_devices(md);
	return 0;
}

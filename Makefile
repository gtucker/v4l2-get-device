CFLAGS := -Iinclude

all: v4l2-get-device

v4l2-get-device: v4l2-get-device.o get_media_devices.o

get_media_devices.c: include/libmedia_dev/get_media_devices.h
v4l2-get-device.c: include/libmedia_dev/get_media_devices.h

install: v4l2-get-device
	install -m0755 v4l2-get-device /usr/local/bin/

.PHONY: clean

clean:
	-rm -f v4l2-get-device
	-rm -f *.o
